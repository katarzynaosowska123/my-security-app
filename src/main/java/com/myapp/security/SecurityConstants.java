package com.myapp.security;

public class SecurityConstants {
    //Secret key used to generate token (jwt)
    public static final String SECRET = "a#@#slolea3ba*%@n242@#$32dfs";
    public static final long EXPIRATION_TIME = 864_000_000; //10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/users/sign-up";
}
