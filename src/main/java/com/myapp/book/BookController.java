package com.myapp.book;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class BookController {

    @GetMapping("/books/getAll")
    public List<Book> getBooks() {
        return Arrays.asList(
                new Book(1, "Pan Tadeusz", "Adam Mickiewicz", 2018),
                new Book(2, "Ja, Klaudiusz", "Robert Graves", 2016),
                new Book(3, "Czesław Miłosz", "Zniewolony umysł", 2005),
                new Book(4, "George Orwell", "Rok 1984", 2010),
                new Book(5, "Quo Vadis", "Henryk Sienkiewicz", 2011)
        );
    }

}
